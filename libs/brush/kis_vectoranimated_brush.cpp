﻿/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "kis_vectoranimated_brush.h"

#include <QDomElement>
#include <QFileInfo>
#include <QPainter>
#include <QImageReader>
#include <QSvgRenderer>

#include <SvgParser.h>
#include <KoDocumentResourceManager.h>
#include <KoXmlReader.h>
#include <KoShapePainter.h>
#include <kis_brushes_pipe.h>

#include <QDebug>

KisVectorAnimatedBrush::KisVectorAnimatedBrush(const QString& filename)
    : KisScalingSizeBrush(filename)
 /*   , m_imageNumber(0)
    , m_isInitialized(false) */
{
    setBrushType(INVALID);
    setSpacing(0.25);
    setHasColor(false);
    /*
    if (!m_isInitialized) {
        if (m_imageNumber ==  m_numberOfImages) {
            m_imageNumber = 0;
            qDebug() << "m_imageNumber" << m_imageNumber;
        } else {
            m_imageNumber ++;
            qDebug() << "m_imageNumber" << m_imageNumber;
        }
    } */
}

KisVectorAnimatedBrush::KisVectorAnimatedBrush(const KisVectorAnimatedBrush& rhs)
    : KisScalingSizeBrush(rhs)
    , m_svg(rhs.m_svg)
{
}

KisBrush* KisVectorAnimatedBrush::clone() const
{
    return new KisVectorAnimatedBrush(*this);
}

bool KisVectorAnimatedBrush::load()
{
    QFile f(filename());
    if (f.size() == 0) return false;
    if (!f.exists()) return false;
    if (!f.open(QIODevice::ReadOnly)) {
        warnKrita << "Can't open file " << filename();
        return false;
    }
    bool res = loadFromDevice(&f);
    f.close();

    return res;
}

bool KisVectorAnimatedBrush::loadFromDevice(QIODevice *dev)
{
    m_svg = dev->readAll();

    KoXmlDocument documento = SvgParser::createDocumentFromSvg(m_svg);
    // qDebug() << documento.toString();

    KoXmlElement elementos = documento.documentElement();

    /* qDebug() << documento.toString();
     qDebug() << elementos.text(); */

    KoDocumentResourceManager manager;
    SvgParser parser(&manager);
    parser.setResolution(QRectF(0,0,100,100), 72); // initialize with default values
    QSizeF fragmentSize;

    // QList<KoShape*> shapes = parser.parseSvg(documento.documentElement(), &fragmentSize);
    QList<KoShape*> list = parser.parseSvg(elementos, &fragmentSize);

    /* // According to documentation it is better to use const_iterator if we won't change the QList elements
    QList<KoShape*>::const_iterator i;
     for (i = list.constBegin(); i != list.constEnd(); ++i)
        qDebug() << "figura" << *i;
        // qDebug() <<  list.at(*i) ;

     for (int i = 0; i != list.size(); ++i) {
         qDebug() << "figura" << i;
         qDebug() <<  list.at(i);
}
*/
    m_imageNumber = 0;
    qDebug() << "Numero:" << m_imageNumber;

    QList<KoShape*> single;
    single.append(list.at(m_imageNumber));

    qDebug() << "lista" << list.count();
    qDebug() << "figura" << m_imageNumber <<  list.at(m_imageNumber);

/*
    if (m_imageNumber ==  list.size()) {
        m_imageNumber = 0;
    }
        else {
        m_imageNumber ++;
    }
    */
    m_numberOfImages = list.size();
    m_isInitialized = true;
    // qDebug() << "numberImages" << m_numberOfImages << "initialized" << m_isInitialized << "numero" << m_imageNumber ;

    KoShapePainter painter;
    painter.setShapes(single);

    QImage theImage(1000, 1000, QImage::Format_ARGB32);  //fix size
    {
        QPainter p(&theImage);
        p.fillRect(0, 0, theImage.width(), theImage.height(), Qt::white);

    }

    painter.paint(theImage);

    theImage.save("my_testImage", "PNG"); //should save only one image

   /* qDebug() << "shapes" << list[0]; */



 // ------------------------------------------- old ------------------------

  /*  QSvgRenderer renderer(m_svg);
    QRect box = renderer.viewBox();
    if (box.isEmpty()) return false;

    QImage image_(1000, (1000 * box.height()) / box.width(), QImage::Format_ARGB32);
    {
        QPainter p(&image_);
        p.fillRect(0, 0, image_.width(), image_.height(), Qt::white);
        renderer.render(&p);
    }
    */

    // ------------------------------------------- old ------------------------

    QVector<QRgb> table;
    for (int i = 0; i < 256; ++i) table.push_back(qRgb(i, i, i));
    theImage = theImage.convertToFormat(QImage::Format_Indexed8, table);

    setBrushTipImage(theImage);
    // Blackbeard TODO!!
    // KoCanvasControllerWidgetViewport_p.cpp
    // KoSvgSymbolCollectionResource.cpp   
    // KoShapePainter.cpp --> boud told me to check this library
    // SvgWriter.cpp line 256
    // KoMarker.cpp line 99
    // setBrushTipImage(theImage);
    // > couldn't you just set a single shape with void setShapes(const
    // QList<KoShape*> &shapes); ?

    setValid(true);

    // Well for now, always true
    if (brushTipImage().isGrayscale()) {
        setBrushType(MASK);
        setHasColor(false);
    }
    else {
        setBrushType(IMAGE);
        setHasColor(true);
    }
    setWidth(brushTipImage().width());
    setHeight(brushTipImage().height());

    QFileInfo fi(filename());
    setName(fi.baseName());

    return !brushTipImage().isNull() && valid();
}

bool KisVectorAnimatedBrush::save()
{
    QFile f(filename());
    if (!f.open(QFile::WriteOnly)) return false;
    bool res = saveToDevice(&f);
    f.close();
    return res;
}

bool KisVectorAnimatedBrush::saveToDevice(QIODevice *dev) const
{
    if((dev->write(m_svg.constData(), m_svg.size()) == m_svg.size())) {
        KoResource::saveToDevice(dev);
        return true;
    }
    return false;
}

QString KisVectorAnimatedBrush::defaultFileExtension() const
{
    return QString(".avb");
}

void KisVectorAnimatedBrush::toXML(QDomDocument& d, QDomElement& e) const
{
    predefinedBrushToXML("svg_brush", e);
    KisBrush::toXML(d, e);
}



int KisVectorAnimatedBrush::currentIndex()
{
    qDebug() << "m_imageNumber" << m_imageNumber;
    return m_imageNumber;
}
